
<h2>Установка и сборка</h2>

версия node.js v18.15.0

# установка зависимостей
$ npm install

# запуск сервера разработки (localhost:5173)
# для dev-стенда
$ npm run dev

# для prod-стенда
$ npm run build
